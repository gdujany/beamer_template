# Name of the main LaTeX file (do not include .tex)
# Ideally, change this and the correspond file to something meaningful,
# e.g. BsToPhiPhi-presentation
NAME = presentation

## Generally, there's no need to edit beyond this line ##

# Name of the directory that contains figures
FIGDIR = figures

# Default LaTeX distribution on lxplus is quite old, so fetch
# a newer one if we're on an lxplus node
HOST = $(shell hostname)
ifneq (,$(findstring lxplus, $(HOST)))
	export PATH := /afs/cern.ch/sw/XML/texlive/latest/bin/x86_64-linux::$(PATH)
endif

# list of all source files
TEXSOURCES = $(wildcard *.tex) $(wildcard *.sty)
FIGSOURCES = $(wildcard $(FIGDIR)/*)
SOURCES = $(TEXSOURCES) $(FIGSOURCES) Makefile

LATEX=pdflatex
LATEXOPT=

LATEXMK=latexmk
LATEXMKOPT=-pdf

.PHONY: all
all: $(NAME).pdf

$(NAME).pdf: $(SOURCES)
	@$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT)" $(NAME)

.PHONY: clean
clean:
	@$(LATEXMK) -c
	rm $(NAME).snm $(NAME).nav

.PHONY: cleanall
cleanall:
	@$(LATEXMK) -C
	rm $(NAME).snm $(NAME).nav
